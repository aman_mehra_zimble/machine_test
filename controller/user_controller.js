const { validateTestimonialPost,validateGetDistance } = require("../models/users");
const User = require("../models/users");
const mongoose = require("mongoose")
const bcrypt = require('bcryptjs');
const saltRounds = 10;
const jwt = require("jsonwebtoken")
const geolib = require("geolib")

module.exports = {
  createUser: async (req, res) => {
    try {
       
          var data = req.body;
          var file = req.files;
          
          // console.log("data", data, file);

          const { error } = await validateTestimonialPost(req.body);
          if (error) return res.status(400).send({ statusCode: 400, message: "Failure", data: { message: error.details[0].message } });

         var validateEmail =  await User.checkeMail(data.email)
          if(validateEmail){

            return res.status(400).send({statusCode:"400", message:"Email already exist"});
          }
          
          data.password =await bcrypt.hash(data.password, saltRounds)
         var token = await jwt.sign({ email:data.email ,latitude:data.latitude,longitude:data.longitude}, 'shhhhh');
          userData = {
            name:data.name,
            email:data.email,
            password:data.password,
            address:data.address,
            latitude:data.latitude,
            longitude:data.longitude,
            token:token

          }
          await User.addUser(userData, async (err, response) => {
            if (err) {
            } else {
              return res.send({ statusCode: 200, message: "Success", data: response });
            }
          });
        
    } catch (error) {
      console.log("error", error);
      return res.json({ message: "Internal Server Error" });
    }
  },

  updateUserStatus: async (req, res) => {
    try {
      if (!req.headers.token) {
        return res.status(400).send({ statusCode: 400, message: "Failure", data: { message: "Token is required." } });
      }
      
    
     var resopv = await User.updateStatus()
     console.log(resopv);
      if(resopv){
    return res.status(200).send({ statusCode: 200, message: "Success",  });
      }
        
      
    } catch (error) {
      console.log(error);
      return res.send({ message: "Internal Server Error" });
    }
  },

  getDistance: async (req, res) => {
    try {
      if (!req.headers.token) {
        return res.status(404).send({
          statusCode: 404,
          message: "Failure",
          data: { message: "Token is required" }
        });
      }
     
      const { error } = await validateGetDistance(req.body);
      if (error) return res.status(400).send({ statusCode: 400, message: "Failure", data: { message: error.details[0].message } });
      
      var verifyData =await jwt.verify(req.headers.token,"shhhhh")
  
      var calDistance = await geolib.getPreciseDistance(
        {
          latitude: Number(verifyData.latitude),
          longitude: Number(verifyData.longitude),
        },
        {
          latitude: Number(req.body.latitude),
          longitude: Number(req.body.longitude),
        }
      );
        console.log("------------",calDistance);
          
          return res.status(200).send({
            statusCode: 200,
            message: "Success",
            distance: calDistance
          });
     
    } catch (error) {
      console.log(error);
      return res.json({ message: "Internal Server Error" });
    }
  },

  getUserList: async (req, res) => {
    try {
        var monday=[];
        var tuesday=[];
        var wednessday=[];
        var thursday=[];
        var friday=[];
        var saturday=[];
        var sunday=[];
        var finalList={}
        await User.getAllDetails(async(err,respo)=>{
          if(err){
            console.log(err);
          }else{
            await respo.forEach(element => {
              if(element.register_at.getDay == 0){
                sunday.push({name:element.name,emil:element.email})
              }else if(element.register_at.getDay() == 1){
                monday.push({name:element.name,emil:element.email})
              }else if(element.register_at.getDay() == 2){
                tuesday.push({name:element.name,emil:element.email});
              }else if(element.register_at.getDay() == 3){
                wednessday.push({name:element.name,emil:element.email})
              }else if(element.register_at.getDay() == 4){
                thursday.push({name:element.name,emil:element.email})
              }else if(element.register_at.getDay() == 5){
                friday.push({name:element.name,emil:element.email})
              }else if(element.register_at.getDay() == 6){
                saturday.push({name:element.name,emil:element.email})
              }else{
                sunday.push({name:element.name,emil:element.email})
              }
            });
           finalList = {
              "Monday":monday,
              "Tuesday":tuesday,
              "Wednesday":wednessday,
              "Thuesday":thursday,
              "Friday":friday,
              "Saturday":saturday,
              "Sunday":sunday
            }
          }
        })
        
        // console.log("finalList",finalList);
        return res.send({ statusCode: 200, message: "Success", data: finalList });
      
    } catch (error) {
      console.log("error", error);

      return res.json({ message: "Internal Server Error" });
    }
  }
};
