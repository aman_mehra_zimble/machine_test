const Joi = require("joi");
const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({
  name: { type: String },
  email: { type: String },
  password: { type: String },
  address: { type: String },
  status: { type: String, default: "active" },
  register_at: { type: Date,default: Date.now, },
  latitude:{type:String},
  longitude:{type:String},
  token:{type:String}
});

const User = (mongoose.model("User", userSchema))
module.exports = {
  validateTestimonialPost: async (userSchema) => {
    const schema = Joi.object({
      name: Joi.string().min(4).max(50).required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
      address: Joi.string().required(),
      latitude: Joi.string().required(),
      longitude: Joi.string().required(),
    });

    return schema.validate(userSchema);
  },
  validateGetDistance: async (userSchema) => {
    const schema = Joi.object({
      latitude: Joi.number().required(),
      longitude: Joi.number().required(),
    });

    return schema.validate(userSchema);
  },
  addUser: async (data, callback) => {
    return User.create(data, callback);
  },
  updateTestimonial: async (query, update, callback) => {
    return Testimonial.findOneAndUpdate(query, update, { new: true }, callback);
  },

  getAllDetails: async (callback) => {
    return await User.find({},callback);
    
  },
  
  checkeMail: async (data) => {
    return User.findOne({ email: data });
  },
  updateStatus : async()=>{
    return await User.updateMany( {pipeline:[
      {
        $set: {
          status: {
            $switch: {
              branches: [
                { case: { "$status":{$eq: "active" }}, then: "inactive" },
                { case: { "$status":{$eq: "inactive" }}, then: "active" },
              ],
              default: "",
            },
          },
        },
      },
    ]
  });
  },
// return User.updateMany({},{ pipeline:[
//     {
//       $set: {
//         status: {
//           $cond: {
//             if: {
              
//               "status":'active'
//             },
//             then: "inactive", 
//             else: 'active', 
//           }
//         }
//       }
//     }
//   ]}
 
// )

    

getDistance: async(data)=>{
  console.log("data: ",data);
  return User.aggregate([
    {
      $near: {
        near: {
          type: "Point",
          coordinates: [74.857025, 32.726601],
        },
        distanceField: "distance",
        spherical: true,
        distanceMultiplier: 6378.1,
      },
    },
  ]);
}

}


