const user_controller = require("../controller/user_controller");
const express = require("express");

const router = express.Router();

router.post("/create", user_controller.createUser);

router.post("/updateStatus", user_controller.updateUserStatus);

router.post("/getDistance", user_controller.getDistance);

router.post("/getUserList", user_controller.getUserList);

module.exports = router;
